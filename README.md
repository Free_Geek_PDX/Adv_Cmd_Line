# This a class for advanced command line skills.

Topics that may be covered are:
Some of the requested topics to cover are:
- Bash shortcuts
- Tab completion
- grep
- chmod/chown
- rsync
- find/locate
- Reading and understanding scripts
- Deeper explanation of how Command Line works

# Reasons for taking the class
- Sometimes an app or the graphical Gnome desktop will “freeze up”, and you can use the CL to kill the frozen app, restart the Gnome desktop, or even reboot your computer.
- Some linux computers don’t even run a graphical desktop environment, and everything is done with the CL. Many web app servers and database servers running Linux will not have a graphical desktop installed or running (this helps free up resources - memory).
- If you want to be a Linux server and/or network administrator, then you will need to have good CL skills.
- Some things are done faster in the CL than the graphical Gnome desktop.
- You can really impress that hot nerdy girl or guy with your geeky CL skills!

# Class resources
To get the class resources execute the following
commands, you do not have to type the # comments

```bash
git clone https://tinyurl.com/advcmdline

git clone https://gitlab.com/Free_Geek_PDX/Adv_Cmd_Line.git/
```

or if you do not have git installed

```bash
wget -A tar https://goo.gl/kroZEd -O archive.tar # download the files
tar -x -f archive.tar # extract the files
mv Adv_Cmd_Line* class # rename the directory
rm archive.tar # remove the downloaded archive```
```

This will get all the files from the git repository and make them available in the class directory

At this point you can follow along with the class with a browser. Open this file in the `html` directory in class.

 - [command line](Command_line.html)
 - [bash shortcuts](Bash_Shortcuts.html)
 - [grep](Grep.html)
 - [find & locate](find.html)
 - [rsync](rsync.html)
 - [scripts](scripts.html)

# resources
 - http://www.tldp.org/guides.html guides for Linux
 - https://beebom.com/essential-linux-commands/ list and description of essential commands
 - https://searchdatacenter.techtarget.com/tutorial/77-Linux-commands-and-utilities-youll-actually-use
 - https://geek-university.com/linux/the-shell-in-linux/ General information on tutorials on Linux, with lots on shell
 - https://www.makeuseof.com/tag/linux-commands-reference-pdf/ A list of common commands
 - http://faculty.ucr.edu/~tgirke/Documents/UNIX/linux_manual.html
