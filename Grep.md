# Grep
The grep command is used to search a file for lines containing a match to the string you specify. The string can use regular expressions and can be used to output the matching lines, the file name, lines before and after.

The name grep comes from the command in text editor `ed` that uses `g/re/p`.

The syntax of the command is
```
grep 'words' filename
```
If you use a directory instead of a filename and use the switch `-r` then it searches recursively through the directory and all subdirectories.

Grep only looks at text files unless you use the switch `--binary-files=TYPE` or `-a`, it will search the file for the characters. It is very fast! If you want to search in binary files you should read the man page.

## Matching Control
  -e PATTERN matches the pattern, can be used multiple times

 -f FILE reads patterns from a file

 -i ignore case in pattern

 -w match only whole words

 -x match the full line

## Output Control
-c only print count of matching lines

-L suppress normal output, instead print the name of each input file that doesn't match

-l suppress normal output, instead print the name of each input file that does match

## Context Line Control

`-A NUM` print NUM lines of trailing context after matching lines

`-B NUM` print NUM lines of leading context before matching lines

You can also have actions performed based on the files that are found.

-r read all files under each directory, recursively

## Regular expressions

`[]` is an expression that is a list of characters that match any one character. If the first character in the list is ^ then any character not in the list matches. A `[a-c]` matches all characters between a and c. There are classes defined like `[:alnum:]` for all numbers and letters.

A caret `^` and the dollar sound `$` match the beginning and ending of the line.

### Repetition
An expression can have the following
 - `?` preceding item is optional and matched at most once
 - `*` preceding item can be matched zero or more times
 - `+` preceding item can be matched one or more times
 - `{n}`, `{n,}`, `{,m}`, and `{n,m}` match exactly n, n or more, at most m, or matched between n and m times.

Two regular expressions can be concatenated

Two regular expressions can be joined by `|` operator

In regular expressions the meta-characters ?, +, {, }, |, (, and ) lose their special meaning; instead use the backslashed versions \?, etc.
