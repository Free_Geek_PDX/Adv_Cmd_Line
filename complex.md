# Examples

## A complex Example
Count the occurrences of words in a text file and list the highest first n words

1) Find a text file
2) Type the following, and there is a return between the first and second single quote. The file you selected should replace `file.txt`.
```
tr -cs A-Za-z '
' < file.txt
```
3) When you run this command you will get a list of the words in the file as the -cs command combines the letters together and changes all the spaces to returns
4) Now execute the following by adding a pipe to sort so the words are sorted in alphabetical order.
```
tr -cs A-Za-z '
' < file.txt | sort
```
5) The output of this is a sorted list of the words in the file. Next add the command `uniq -c` which counts (the -c) the words and puts the count before each word.
```
tr -cs A-Za-z '
' < file.txt | sort | uniq -c
```
6) Now we have the list of words but they are not in the order of occurrences. Add the command `sort -rn` which sorts the list numerically in reverse order.
```
tr -cs A-Za-z '
' < file.txt | sort | uniq -c | sort -rn
````
7) Now all we need to do is curtail the list to the top n words using the sed command. Add `sed 5q` assuming that n is 5. This tells sed to use up 5 lines and then quit.
```
tr -cs A-Za-z '
' < file.txt | sort | uniq -c | sort -rn | sed 5q
````

`
