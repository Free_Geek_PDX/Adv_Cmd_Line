#!/bin/bash
rsync -aruvh --del --progress ~/Pictures /media/byron/Backup
rsync -aruvh --del --progress ~/Audiobooks /media/byron/Backup
rsync -aruvh --del --progress ~/bin /media/byron/Backup
rsync -aruvh --del --progress ~/dev /media/byron/Backup
rsync -aruvh --del --progress ~/IdeaProjects /media/byron/Backup
rsync -aruvh --del --progress ~/Music /media/byron/Backup
